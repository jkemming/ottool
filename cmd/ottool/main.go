package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"ottool"
	"ottool/internal/log"
	"ottool/internal/postgres"

	"github.com/urfave/cli/v2"
)

func main() {
	config, err := getConfig()
	if err != nil {
		fmt.Println("Error getting config:", err)
		os.Exit(1)
	}

	app := cli.NewApp()
	app.Name = "ottool"

	app.Commands = []*cli.Command{
		{
			Name:    "postgres",
			Aliases: []string{"pg"},
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name:     "env",
					Aliases:  []string{"e"},
					Required: true,
				},
			},
			Subcommands: []*cli.Command{
				{
					Name:    "dump",
					Aliases: []string{"d"},
					Action:  setLogLevel(postgres.Dump(config)),
				},
				{
					Name:    "restore",
					Aliases: []string{"r"},
					Action:  setLogLevel(postgres.Restore(config)),
				},
				{
					Name:    "tunnel",
					Aliases: []string{"t"},
					Action:  setLogLevel(postgres.Tunnel(config)),
				},
			},
		},
	}
	app.Flags = []cli.Flag{
		&cli.BoolFlag{
			Name:    "verbose",
			Aliases: []string{"v"},
		},
	}

	if err := app.Run(os.Args); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func getConfig() (*ottool.Config, error) {
	configDirPath, err := os.UserConfigDir()
	if err != nil {
		return nil, errors.New(fmt.Sprint("Error getting user config dir:", err))
	}

	configPath := configDirPath + "/ottool/config.json"
	if _, err := os.Stat(configPath); os.IsNotExist(err) {
		return nil, errors.New(fmt.Sprint("Config file not found at ", configPath))
	}

	configBytes, err := ioutil.ReadFile(configPath)
	if err != nil {
		return nil, errors.New(fmt.Sprint("Error reading config file:", err))
	}

	var config ottool.Config
	err = json.Unmarshal(configBytes, &config)
	if err != nil {
		return nil, errors.New(fmt.Sprint("Error parsing config file:", err))
	}

	return &config, err
}

func setLogLevel(next cli.ActionFunc) cli.ActionFunc {
	return func(c *cli.Context) error {
		if c.Bool("verbose") {
			log.ActiveLevel = log.LevelDebug
		} else {
			log.ActiveLevel = log.LevelInfo
		}
		return next(c)
	}
}
