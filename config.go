package ottool

type Config struct {
	Postgres PostgresConfig `json:"postgres"`
}

type PostgresConfig struct {
	BinPath  string                       `json:"bin_path"`
	DumpPath string                       `json:"dump_path"`
	Envs     map[string]PostgresEnvConfig `json:"envs"`
}

type PostgresEnvConfig struct {
	Host     string            `json:"host"`
	Password string            `json:"password"`
	Port     string            `json:"port"`
	Ssh      PostgresSshConfig `json:"ssh"`
	Username string            `json:"username"`
}

type PostgresSshConfig struct {
	Ec2Name string `json:"ec2_name"`
	KeyPath string `json:"key_path"`
	RdsName string `json:"rds_name"`
}
