package aws

import (
	"bytes"
	"errors"
	"os/exec"
	"strings"
)

func GetEc2Ip(name string) (string, error) {
	cmd := exec.Command("aws",
		"ec2",
		"describe-instances",
		"--filters", "Name=tag:Name,Values="+name,
		"--output", "text",
		"--query", "Reservations[*].Instances[*].PrivateIpAddress")
	var stdout bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	if cmd.Run() != nil {
		return "", errors.New(stderr.String())
	}
	return strings.TrimSpace(stdout.String()), nil
}

func GetRdsHost(name string) (string, error) {
	cmd := exec.Command("aws",
		"rds",
		"describe-db-instances",
		"--db-instance-identifier", name,
		"--output", "text",
		"--query", "DBInstances[*].Endpoint.Address")
	var stdout bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	if cmd.Run() != nil {
		return "", errors.New(stderr.String())
	}
	return strings.TrimSpace(stdout.String()), nil
}
