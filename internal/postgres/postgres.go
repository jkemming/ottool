package postgres

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"os/exec"
	"ottool"
	"ottool/internal/aws"
	"ottool/internal/log"

	"github.com/urfave/cli/v2"
)

func Dump(config *ottool.Config) cli.ActionFunc {
	return func(c *cli.Context) error {
		env := c.String("env")
		envConfig, ok := config.Postgres.Envs[env]
		if !ok {
			return errors.New(fmt.Sprint("Unknown env: ", env))
		}

		log.Info("Starting dump")
		cmd := exec.Command(config.Postgres.BinPath+"/pg_dump",
			"--file", config.Postgres.DumpPath,
			"--format", "c",
			"--host", envConfig.Host,
			"--port", envConfig.Port,
			"--username", envConfig.Username,
			"arteria")
		cmd.Env = append(cmd.Env, "PGPASSWORD="+envConfig.Password)
		var stderr bytes.Buffer
		cmd.Stderr = &stderr

		if cmd.Run() != nil {
			return errors.New(fmt.Sprint("Error dumping database: ", stderr.String()))
		}
		return nil
	}
}

func Restore(config *ottool.Config) cli.ActionFunc {
	return func(c *cli.Context) error {
		env := c.String("env")
		envConfig, ok := config.Postgres.Envs[env]
		if !ok {
			return errors.New(fmt.Sprint("Unknown env: ", env))
		}

		log.Info("Starting restore")
		cmd := exec.Command(config.Postgres.BinPath+"/pg_restore",
			"--clean",
			"--create",
			"--dbname", "postgres",
			"--host", envConfig.Host,
			"--no-privileges",
			"--port", envConfig.Port,
			"--username", envConfig.Username,
			config.Postgres.DumpPath)
		cmd.Env = append(cmd.Env, "PGPASSWORD="+envConfig.Password)
		var stderr bytes.Buffer
		cmd.Stderr = &stderr

		if cmd.Run() != nil {
			return errors.New(fmt.Sprint("Error restoring database: ", stderr.String()))
		}
		return nil
	}
}

func Tunnel(config *ottool.Config) cli.ActionFunc {
	return func(c *cli.Context) error {
		env := c.String("env")
		envConfig, ok := config.Postgres.Envs[env]
		if !ok {
			return errors.New(fmt.Sprint("Unknown env: ", env))
		}

		log.Info("Getting EC2 IP")
		ec2Ip, err := aws.GetEc2Ip(envConfig.Ssh.Ec2Name)
		if err != nil {
			return errors.New(fmt.Sprint("Error getting EC2 IP: ", err))
		}

		log.Info("Getting RDS host")
		rdsHost, err := aws.GetRdsHost(envConfig.Ssh.RdsName)
		if err != nil {
			return errors.New(fmt.Sprint("Error getting RDS host: ", err))
		}

		log.Info("Starting tunnel")
		// TODO Cancel context on SIGINT etc.
		ctx := context.Background()
		cmd := exec.CommandContext(ctx, "ssh",
			"-i", envConfig.Ssh.KeyPath,
			"-L", envConfig.Port+":"+rdsHost+":"+envConfig.Port,
			"-N",
			"ec2-user@"+ec2Ip)

		go cmd.Run()
		<-ctx.Done()

		return nil
	}
}
