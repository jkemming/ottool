package log

import "fmt"

type Level uint

const (
	LevelDebug Level = iota
	LevelInfo  Level = iota
)

var ActiveLevel Level = LevelInfo

func Debug(a ...interface{}) {
	log(LevelDebug, a...)
}

func Info(a ...interface{}) {
	log(LevelInfo, a...)
}

func log(level Level, a ...interface{}) {
	if ActiveLevel <= level {
		fmt.Println(a...)
	}
}
